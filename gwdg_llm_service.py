import llm
import sys
import os
import json
from getpass import getpass
import requests
from bs4 import BeautifulSoup

BACKEND_URL="https://chat-ai.academiccloud.de/chat-ai-backend"
FRONTEND_URL="https://chat-ai.academiccloud.de/"
MODELS_URL="https://chat-ai.academiccloud.de/models/"

class AuthenticationException(Exception):
    pass

def get_token(session, username: str, password: str):
    def submit_form(form, **kwargs):
        url = kwargs.get("url", form["action"])
        data = kwargs.get("data", {})
        prefilled_data = {
            input.get("name"): input.get("value")
            for input in form.find_all("input")
        }
        response = session.post(url, prefilled_data | data)
        response.raise_for_status()
        return response

    session.cookies.clear() # leftover cached cookies could mess up the authentication

    chatai_response = session.get(FRONTEND_URL)
    chatai_response.raise_for_status()

    chatai_page = BeautifulSoup(chatai_response.text, "html.parser")
    sso_response = submit_form(chatai_page.form)

    sso_page = BeautifulSoup(sso_response.text, "html.parser")

    sso_after_username_input = submit_form(
        sso_page.find("form", {"class": "login"}),
        url=sso_response.url,
        data={
            "login": username,
            "keep_signed_in": "Yes",
        },
    )

    sso_password_page = BeautifulSoup(sso_after_username_input.text, "html.parser")
    if error_message := sso_password_page.find("div", {"class": "error-msg"}):
        raise AuthenticationException(error_message.text.strip())

    sso_postlogin = submit_form(sso_password_page.form, data={"password": password})

    sso_postlogin_page = BeautifulSoup(sso_postlogin.text, "html.parser")
    if error_message := sso_postlogin_page.find("div", {"class": "error"}):
        raise AuthenticationException(error_message.text.strip())

    submit_form(sso_postlogin_page.form)


def plugin_dir():
    return llm.user_dir() / "gwdg"


def authenticate(session):
    username = os.environ.get("GWDG_USERNAME")
    password = os.environ.get("GWDG_PASSWORD")

    try:
        with open(plugin_dir() / "account.json", "r") as login_json:
            plugin_dir().mkdir(exist_ok=True)
            login_data = json.load(login_json)
            username = username or login_data.get("username")
            password = password or login_data.get("password")
    except:
        pass
        
    if not username or not password:
        raise llm.ModelError("Using this model requires setting a username and password.\n\nTo set the username and password run this command:\n\n    llm gwdg login\n\nYou can also use the GWDG_USERNAME and GWDG_PASSWORD environment variables.")

    get_token(session, username, password)


def get_cached_token():
    with open(plugin_dir() / "token", "r") as token_file:
        return token_file.read()

def cache_token(session):
    plugin_dir().mkdir(exist_ok=True)
    with open(plugin_dir() / "token", "w+") as token_file:
        token_file.write(session.cookies["mod_auth_openidc_session"])

@llm.hookimpl
def register_commands(cli):
    @cli.group(name="gwdg")
    def openai_():
        "Commands for working with the GWDG LLM service"

    @openai_.command()
    def login():
        "Setup account information"

        username = input("username: ")
        password = getpass(prompt="password: ")

        plugin_dir().mkdir(exist_ok=True)
        with open(plugin_dir() / "account.json", "w+") as login_json:
            login_json.write('{"username": "%s", "password": "%s"}' % (username, password))


@llm.hookimpl
def register_models(register):
    with requests.Session() as session:

        def get_model_list():
            return session.get(MODELS_URL).json()["data"]

        try:
            token = get_cached_token()
            session.cookies.update(
                requests.utils.cookiejar_from_dict({"mod_auth_openidc_session": token})
            )
            available_models = get_model_list()
        except:
            try:
                authenticate(session)
                cache_token(session)
                available_models = get_model_list()
            except:
                print(
                    "Could not fetch GWDG ChatAI model list. Did you forget to run 'llm gwdg login'?",
                    file=sys.stderr,
                )
                return

        for i, model in enumerate(available_models):
            register(
                GWDGModel("gwdg-" + model["id"], "GWDG ChatAI: " + model["name"], model["id"]),
                aliases=["chatai"] if i == 0 else [],
            )

class GWDGModel(llm.Model):
    can_stream = True

    def __init__(self, model_id, model_description, model_api_name):
        self.model_id = model_id
        self.model_description = model_description
        self.model_api_name = model_api_name

    def execute(self, prompt, stream, response, conversation):
        with requests.Session() as session:
            def send_request():
                messages = []
                current_system = None
                if conversation is not None:
                    for prev_response in conversation.responses:
                        if (
                            prev_response.prompt.system
                            and prev_response.prompt.system != current_system
                        ):
                            messages.append(
                                {"role": "system", "content": prev_response.prompt.system}
                            )
                            current_system = prev_response.prompt.system
                        messages.append(
                            {"role": "user", "content": prev_response.prompt.prompt}
                        )
                        messages.append({"role": "assistant", "content": prev_response.text()})
                if prompt.system and prompt.system != current_system:
                    messages.append({"role": "system", "content": prompt.system})
                messages.append({"role": "user", "content": prompt.prompt})
                request = {
                    "messages": messages,
                    "model": self.model_api_name,
                    "temperature": 1,
                }

                response = session.post(BACKEND_URL, json=request, stream=stream)
                response.raise_for_status()
                
                if response.headers.get("Content-Type") == "text/html;charset=utf-8":
                    # We probably got redirected to the login page
                    raise AuthenticationException("Something went wrong with the authentication.")

                if response.headers.get("Content-Type", "").startswith("application/json"):
                    response = json.loads(response.text)
                    if response.get("error"):
                        raise Exception("Server returned an error: " + response["error"])

                response.encoding = "utf-8" # The backend doesn't set the proper headers
                if stream:
                    yield from response.iter_content(decode_unicode=True)
                else:
                    yield response.text

            try:
                token = get_cached_token()
                session.cookies.update(
                    requests.utils.cookiejar_from_dict(
                        {"mod_auth_openidc_session": token}
                    )
                )
                yield from send_request()
            except:
                authenticate(session)
                cache_token(session)
                yield from send_request()


    def __str__(self):
        return "{}: {}".format(self.model_description, self.model_id)
