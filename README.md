# Unofficial GWDG LLM Service plugin for the `llm` CLI tool

DISCLAIMER: This plugin not associated with or supported by the GWDG. Please do not bother them if this stops working.

This is a plugin for Simon Willison's `llm` [command line tool](https://llm.datasette.io) that makes it very simple to use the [LLM services offered by GWDG](https://chat-ai.academiccloud.de/) from the command line.

## Installation

Install `llm` via `pipx`. You can probably get `pipx` from your distribution's package manager.

```
pipx install llm
```

Then, install the plugin:

```
llm install 'https://gitlab.gwdg.de/api/v4/projects/36321/repository/archive.zip'
```

Now you should see the models offered by GWDG in the list when you run `llm models`.

To use the models, you need to log in with your GWDG account. Run this command to create a config file with your account data:

```
llm gwdg login
```

Alternatively, you can use the `GWDG_USERNAME` and `GWDG_PASSWORD` environment variables.

You should now be able to use the models by passing the `-m` flag:

```
$ echo hello | llm -m gwdg-intel-neural-chat-7B
Hello there! How can I help you today?
```

Alternatively you can set one of the models as default:

```
llm models default gwdg-llama-3-70b-instruct
```

